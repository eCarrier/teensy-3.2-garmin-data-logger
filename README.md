# Teensy 3.2 garmin data logger

Small Teensy 3.2 firmware for testing the Garmin LIDAR-Lite v4. Writes the read data to an SD card and also shows it on a 2 lines LCD screen.

# Hardware
* Teensy 3.2
* Teensy SD card shield
* 1602 LCD
* Garmin LIDAR-Lite v4
  
 
