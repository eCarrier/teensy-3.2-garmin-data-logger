/*
 * garmin_data_logger
 * 
 * Reads from a Garmin LIDAR-Lite v4 LED sensor, prints it on a 1602 LCD (Hitachi HD44780)
 * and writes it to a SD card
 * 
 * --------------------------------------------
 * HARDWARE
 * 
 * * Teensy 3.2
 * * Teensy SD card shield
 * * 1602 LCD
 * * Garmin LIDAR-Lite v4
 * 
 * 
 * Created 2020/05/22
 * 
 * by Étienne Carrier
 */
#define serialCom Serial2
#define USB Serial
#include <LiquidCrystal.h>
#include "SD.h"
#include "SPI.h"
#include "Wire.h"
/* ********************
 * PINOUT
 * *******************
 */
// SD card shield
const byte SDCARD_CS_PIN      = 4;
const byte SDCARD_MOSI_PIN    = 11;
const byte SDCARD_SCK_PIN     = 13;
// I2C stuff for the garmin lidar
const byte GARMIN_I2C_ADDRSS  = 98;
const byte SDA_PIN            = 18;
const byte SCL_PIN            = 19;
// LCD screen control
const byte RS_PIN             = 8;
const byte ENABLE_PIN         = 7;
const byte D4_PIN             = 6;
const byte D5_PIN             = 5;
const byte D6_PIN             = 3;
const byte D7_PIN             = 2;

/*
 * Warning :
 * Note on sensor and screen wiring
 * 
 * GARMIN sensor:
 *      I2C communication on teensy 3.0 through 3.6 require external pullup 4.7k resistors on both SDA and SCL (voir https://www.pjrc.com/teensy/td_libs_Wire.html)
 * 
 * LCD Screen:
 *      You need to use a 10k pot with the middle pin connected to pin 3 of the screen, one on the 5V and the other to GND
 */
const int LOG_BUFF_SIZE = 50;
char *logBuffer;

LiquidCrystal lcd(RS_PIN, ENABLE_PIN, D4_PIN, D5_PIN, D6_PIN, D7_PIN); 

void setup()
{
  //Begin serial communication
  USB.begin(9600);
  //Small delay for power on garmin lidar
  delay(22);
  //Begin I2C communication
  Wire.begin( );
  Wire.setSDA(18);
  Wire.setSCL(19);
  // Allocate memory once for the log buffer
  logBuffer = (char *)malloc(LOG_BUFF_SIZE*sizeof(char));
  logBuffer[0]='\0';
  //Setup sd card
  
  SPI.setMOSI(SDCARD_MOSI_PIN);
  SPI.setSCK(SDCARD_SCK_PIN);
  while (!(SD.begin(SDCARD_CS_PIN))) {
    Serial.println("Unable to access SD card!");
    delay(500);
  }  
  lcd.begin(16, 2); 
  
 
}


void loop(){
  int read_data = 0;
  //Read the distance
  read_data = read_distance();
  //Print the result to screen
  lcd.setCursor(0, 1);
  Serial.println(read_data);
  write_int_to_file(&logBuffer, read_data);
  lcd.setCursor(0, 0);
  lcd.print("Garmin sens. val.");
  lcd.setCursor(0, 1);
  //This cleans the line
  lcd.print("           ");
  lcd.setCursor(0, 1);
  lcd.print(read_data);
  lcd.print(" cm");
}

/**
 * @brief read_distance reads the value from the garmin lite v4 
 */
int read_distance() {
  
  byte read_buffer[4];
  int data = -1;
  // Steps to read the sensor is detailed here
  // https://static.garmin.com/pumac/LIDAR-Lite%20LED%20v4%20Instructions_EN-US.pdf
  //-> Write 0x04 to register 0x00
  write_to_register(GARMIN_I2C_ADDRSS, 0x00, 0x04);
  //-> Read register 0x01.
  read_register(GARMIN_I2C_ADDRSS, 0x01, 1, read_buffer);
  //-> Repeat step 2 until bit 0 (LSB) goes low
  while (byte(read_buffer[0] << 7) != 0) //The read buffer is casted to int because for some fucking reason the bitshift converts uint8_t to int
  {
    read_register(GARMIN_I2C_ADDRSS, 0x01, 1, read_buffer);
  }
  //-> Read two bytes from 0x10 (low byte 0x10 then high byte 0x11) to obtain the 16-bit measured distance in centimeters.
  read_register(GARMIN_I2C_ADDRSS, 0x10, 2, read_buffer);
  //Quick bitshift in data to do that
  data = read_buffer[1] << 8 | read_buffer[0];
  return data;
}

/**
 * @brief move_register_pointer Moves the register pointer of the device with the addrss 
 * 
 * Basically with this device you have have multiple registers and a register pointer that move so
 * your able to request da
 * 
 * @param addrss The address of a device
 * @param reg The register to move to
 */
void move_register_pointer(byte addrss, byte reg)
{
  Wire.beginTransmission((int)addrss);
  Wire.write((int)reg);
  Wire.endTransmission();
}

void write_to_register(byte addrss, byte reg, byte value_to_write)
{
  Wire.beginTransmission(addrss);
  Wire.write(reg);
  Wire.write(value_to_write);
  Wire.endTransmission();
}

/**
 * Read count byte from the register into the dest pointer
 */
void read_register(byte addrss, byte reg, byte count, uint8_t *dest)
{
  int index = 0;
  move_register_pointer(addrss, reg);
  Wire.requestFrom(addrss, count);
  for (int i = 0 ; i < count && Wire.available(); ++i)
  {
    dest[i] = Wire.read();
  }
}

void write_int_to_file(char *logBuffer[], int value) 
{
	//Buffer that represent the value of the sensor
	char valueBuffer[8];
	//Put the 
	sprintf(valueBuffer, "%d\n", value);
	strcat(*logBuffer, valueBuffer);
	File logFile = SD.open("LOG.txt", FILE_WRITE);
	if (logFile)
	{
		logFile.write(*logBuffer, strlen(*logBuffer));
		logFile.close();
	}
	memset(*logBuffer, 0, LOG_BUFF_SIZE );
}
